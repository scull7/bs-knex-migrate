open Ava;

let dirname = switch([%bs.node __dirname]) {
| None => failwith("Not being run in a Node.js context")
| Some(dir) => dir
};

let dbPath = Node.Path.resolve(dirname, "db/dev.sqlite3");

Js.log2("DB PATH: ", dbPath);

let db = Sqlite.Connection.make(
  ~path=dbPath,
  ()
);

let migrate = KnexMigrate.Custom.make(
  ~cwd=dirname,
  ~env="development",
  (),
);

let getTables = (db) =>
  Sqlite.Connection.prepare(
    db,
    {|
      SELECT name
      FROM sqlite_master
      WHERE type = 'table'
    |}
  )
  |. Sqlite.Statement.get([||]);

Async.test(ava, "should run all of the migrations", t => {
  KnexMigrate.Custom.up(migrate, ())
  |> Js.Promise.then_(_ => getTables(db) |> Js.Promise.resolve)
  |> Js.Promise.then_(tables => {
    Js.log2("TABLES: ", tables);
    Test.pass(t);
    Test.finish(t);
    Js.Promise.resolve(());
  })
  |> Js.Promise.catch(e => {
    Test.fail(~message=Js.String.make(e), t);
    Test.finish(t);
    Js.Promise.resolve(());
  })
  |. ignore
});
