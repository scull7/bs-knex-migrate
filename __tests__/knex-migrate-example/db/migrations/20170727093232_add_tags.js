
exports.up = function addTagTable (knex) {
  return knex.schema.createTable('TAG', table => {
    table.increments('ID').primary()
    table.string('KEY').notNullable()
    table.string('VALUE').notNullable()
  })
}

exports.down = function dropTagTable (knex) {
  return knex.schema.dropTableIfExists('TAG')
}
