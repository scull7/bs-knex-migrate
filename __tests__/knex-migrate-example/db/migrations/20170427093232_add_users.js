
exports.up = function addUserTable (knex) {
  return knex.schema.createTable('USER', table => {
    table.increments('ID').primary()
    table.string('NAME').notNullable()
  })
}

exports.down = function dropUserTable (knex) {
  return knex.schema.dropTableIfExists('USER')
}
