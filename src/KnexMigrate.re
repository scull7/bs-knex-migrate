
module Wrapper = {
  module Flags = {
  [@bs.deriving abstract]
    type t = {
      [@bs.optional] name: string,
      [@bs.optional] to_: string,
      [@bs.optional] from: string,
      [@bs.optional] only: string,
      [@bs.optional] step: string,
      [@bs.optional] cwd: string,
      [@bs.optional] knexfile: string,
      [@bs.optional] env: string,
      [@bs.optional] raw: bool,
      [@bs.optional] verbose: bool,
    };
  };

  module Status = {
    type t = {
      action: string,
      migration: string,
    };

    let debug = Debug.make("knex-migrate", "Wrapper.Status");

    let log ({ action, migration }) =
      debug({j|Doing $action on $migration|j});
  };

  [@bs.module]
  external run :
    (
      ([@bs.string]
       [ `generate
       | `pending
       | `list
       | `up
       | `down
       | `rollback
       | `redo
       ]
      ),
      Flags.t,
      (Status.t => unit),
    ) => Js.Promise.t(unit)
    = "knex-migrate";
};

module GlobalFlags = {
  [@bs.deriving abstract]
  type t = {
    [@bs.optional] cwd: string,
    [@bs.optional] knexfile: string,
    [@bs.optional] env: string,
    [@bs.optional] raw: bool,
    [@bs.optional] verbose: bool,
  };

  let toFlags = (~name=?, ~to_=?, ~from=?, ~only=?, ~step=?, t) =>
    Wrapper.Flags.t(
      ~name?,
      ~to_?,
      ~from?,
      ~only?,
      ~step?,
      ~cwd=?t |. cwdGet,
      ~knexfile=?t |.knexfileGet,
      ~env=?t |.envGet,
      ~raw=?t |. rawGet,
      ~verbose=?t |. verboseGet,
      ()
    );

  let make = t;

  let empty = () |. t;

};

let generate = (~logger=Wrapper.Status.log, ~flags=GlobalFlags.empty, name) => {
  let flags = GlobalFlags.toFlags(~name, flags);

  Wrapper.run(`generate, flags, logger);
};

let pending = (~logger=Wrapper.Status.log, ~flags=GlobalFlags.empty, _) => {
  let flags = GlobalFlags.toFlags(flags);

  Wrapper.run(`pending, flags, logger);
};

let list = (~logger=Wrapper.Status.log, ~flags=GlobalFlags.empty, _) => {
  let flags = GlobalFlags.toFlags(flags);

  Wrapper.run(`list, flags, logger);
};

let redo = (~logger=Wrapper.Status.log, ~flags=GlobalFlags.empty, _) => {
  let flags = GlobalFlags.toFlags(flags);

  Wrapper.run(`redo, flags, logger);
};

let up = (
  ~logger=Wrapper.Status.log,
  ~flags=GlobalFlags.empty,
  ~to_=?,
  ~from=?,
  ~only=?,
  ~step=?,
  _
) => {
  let flags = GlobalFlags.toFlags(~to_?, ~from?, ~only?, ~step?, flags);

  Wrapper.run(`up, flags, logger);
};

let down = (
  ~logger=Wrapper.Status.log,
  ~flags=GlobalFlags.empty,
  ~to_=?,
  ~from=?,
  ~only=?,
  ~step=?,
  _
) => {
  let flags = GlobalFlags.toFlags(~to_?, ~from?, ~only?, ~step?, flags);

  Wrapper.run(`down, flags, logger);
};

let rollback = (~logger=Wrapper.Status.log, ~flags=GlobalFlags.empty, _) => {
  let flags = GlobalFlags.toFlags(flags);

  Wrapper.run(`rollback, flags, logger);
};

module Custom = {
  type t = {
    flags: GlobalFlags.t,
    logger: (Wrapper.Status.t => unit),
  };

  let make = (~logger=Wrapper.Status.log, ~cwd=?, ~knexfile=?, ~env=?, ~raw=?, ~verbose=?, _) => {
    flags: GlobalFlags.make(~cwd?, ~knexfile?, ~env?, ~raw?, ~verbose?, ()),
    logger,
  };

  let generate = ({ logger, flags }, name) => generate(~logger, ~flags, name);

  let pending = ({ logger, flags }) => pending(~logger, ~flags, ());

  let list = ({ logger, flags }) => list(~logger, ~flags, ());

  let redo = ({ logger, flags }) => redo(~logger, ~flags, ());

  let up = (
    { logger, flags },
    ~to_=?,
    ~from=?,
    ~only=?,
    ~step=?,
    _
  ) => {
    let result = up(~logger, ~flags, ~to_?, ~from?, ~only?, ~step?, ());
    result;
  };

  let down = (
    { logger, flags },
    ~to_=?,
    ~from=?,
    ~only=?,
    ~step=?,
    _
  ) => down(~logger, ~flags, ~to_?, ~from?, ~only?, ~step?, ());

  let rollback = ({ logger, flags }) => rollback(~logger, ~flags, ());

};
